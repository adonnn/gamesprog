#ifndef GAME_H
#define GAME_H
#include "GameState.h"
#include "MenueGameState.h"
#include "HighScores.h"
#include "AboutClass.h"
#include "HelpClass.h"
#include "AdventureClass.h"
#include <string>
using namespace std;
class Game
{
public: void runGame() {
	string input;
	input = "";
	this->currentState = new MenueGameState();
	while (input != "Quit") {

		this->currentState->Draw();
		this->currentState->Update();
		this->currentState->~GameState();
		delete currentState;
		cin >> input;
		if (input == "Highscores") { this->currentState = new HighScoresstate(); }
		else if (input == "About") { this->currentState = new AboutClass(); }
		else if (input == "Help") { this->currentState = new HelpClass(); }
		else if (input == "Adventure") { this->currentState = new HighScoresstate(); }
		else if (input == "Gameplay") { this->currentState = new HighScoresstate(); }
		else if (input == "New Highscores") { this->currentState = new HighScoresstate(); }

		else { this->currentState = new MenueGameState(); }


	}
}
private: GameState *currentState;
};

#endif