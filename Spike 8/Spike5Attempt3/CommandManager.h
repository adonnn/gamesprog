#pragma once
#include <iostream>
#include <map>
#include <string>
#include <iterator>
#include <vector>
#include <sstream>
#include "inventoryclass.h"
#include "TakeCommand.h"
#include "GoCommand.h"
#include "UseCommand.h"
using namespace std;
class CommandManager
{
public:
	inventoryclass player;
	map<string, BaseCommand*> cmds;

	void doStuff(string input);
	CommandManager();
	~CommandManager();
};

