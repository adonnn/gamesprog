#pragma once
#include "BaseCommand.h"
#include "inventoryclass.h"
#include "ItemClass.h"
#include <vector>
#include <iostream>
class TakeCommand: public BaseCommand, public inventoryclass
{
public:
	void process(vector<string> bits);
	TakeCommand();
	~TakeCommand();
};

