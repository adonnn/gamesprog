#include "GoCommand.h"

void GoCommand::process(vector<string> bits)
{
	vector <gamestage> statesofgame;
	gamestage* house1;
	gamestage house;
	gamestage* pool1;
	gamestage pool;
	gamestage* backhouse1;
	gamestage backhouse;
	stagestate statemanager;
	fileread file;
	int increment = 0;

	house1 = &house;
	pool1 = &pool;
	backhouse1 = &backhouse;
	file.filereads(*house1, increment);
	file.~fileread();
	increment++;
	file.filereads(*pool1, increment);
	increment++;
	file.~fileread();
	file.filereads(*backhouse1, increment);
	increment++;
	file.~fileread();


	statesofgame.push_back(*house1);
	statesofgame.push_back(*pool1);
	statesofgame.push_back(*backhouse1);
	

	if (bits[1] == "house") {
		statemanager.printstage(statesofgame[0]);
		statemanager.previewstage(statesofgame[1], statesofgame[2]);
	}
	if (bits[1] == "backhouse") {
		statemanager.printstage(statesofgame[2]);
		statemanager.previewstage(statesofgame[0], statesofgame[1]);
	}
	if (bits[1] == "pool") {
		statemanager.printstage(statesofgame[1]);
		statemanager.previewstage(statesofgame[0], statesofgame[2]);
	}
}

GoCommand::GoCommand()
{
}


GoCommand::~GoCommand()
{
}
