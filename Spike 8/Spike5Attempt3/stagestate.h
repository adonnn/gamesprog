#pragma once
#include "gamestage.h"
#include <iostream>
#include <string>
#include <list>
#include <typeinfo>
#include <vector>
#include <map>
#include <iterator>
#include <sstream>

using namespace std;
class stagestate
{
public:
	string input = "0";
	vector <gamestage> stage;

	void Draw();
	void printstage(gamestage &statesofgame);
	void addStage(gamestage &thestage);
	void previewstage(gamestage &statesofgame, gamestage &statesofgame2);
	stagestate();
	~stagestate();

};