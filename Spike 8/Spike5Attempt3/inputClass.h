#pragma once
#include <map>
#include <string>
#include "verb.h"
#include "Noun.h"
class inputClass
{
public:
	inputClass();
	~inputClass();
	std::map<std::string, verb > knownVerbs;
	std::map<std::string, Noun> knownNouns;
	verb parseVerb(std::string& word);
	Noun parseNoun(std::string& word);
};
