#ifndef GAME_H
#define GAME_H
#include "GameState.h"
#include "MenueGameState.h"
#include "HighScores.h"
#include "AboutClass.h"
#include "HelpClass.h"
#include "AdventureClass.h"
#include <string>
using namespace std;
class Game
{
public: void runGame() {
	string input;
	input = "";
	this->currentState = new MenueGameState();
	while (input != "Quit") {

		this->currentState->Draw();
		this->currentState->Update();
		this->currentState->~GameState();
		delete currentState;
		cout << "type Quit to quit the game." << endl;
		cin >> input;
		if (input == "Highscores") { this->currentState = new HighScoresstate(); }
		else if (input == "About") { this->currentState = new AboutClass(); }
		else if (input == "Help") { this->currentState = new HelpClass(); }
		else if (input == "Adventure") { this->currentState = new AdventureClass(); }
		else if (input == "Gameplay") { this->currentState = new AdventureClass(); }
		else if (input == "newHighscores") { this->currentState = new HighScoresstate(); }

		else {
			cout << "Please input a correct input...\n"; 
			this->currentState = new MenueGameState(); }


	}
}
private: GameState *currentState;
};

#endif