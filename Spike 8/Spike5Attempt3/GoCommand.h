#pragma once
#include "BaseCommand.h"
#include "stagestate.h"
#include "gamestage.h"
#include <vector>
#include <iostream>
#include "fileread.h"
using namespace std;
class GoCommand : public BaseCommand
{
public:

	void process(vector<string> bits);
	GoCommand();
	~GoCommand();
};

