#pragma once
#include <vector>
#include <string>
class BaseCommand
{
public:
	virtual void process(std::vector<std::string> bits) = 0;
	BaseCommand();
	~BaseCommand();
};

