#include "inputClass.h"

inputClass::inputClass()
{

}

inputClass::~inputClass()
{

}

verb inputClass::parseVerb(std::string& word)
{

	knownVerbs["take"] = verb::take;
	knownVerbs["go"] = verb::go;
	

	auto w = knownVerbs.find(word);
	if (w == knownVerbs.end())
		return verb::invalid;

	return w->second;
}

Noun inputClass::parseNoun(std::string& word)
{
	knownNouns["House"] = Noun::house;
	knownNouns["house"] = Noun::house;
	knownNouns["HOUSE"] = Noun::house;
	knownNouns["backyard"] = Noun::backhouse;
	knownNouns["backhouse"] = Noun::backhouse;
	knownNouns["BACKHOUSE"] = Noun::backhouse;
	knownNouns["backdoor"] = Noun::backhouse;
	knownNouns["pool"] = Noun::pool;
	knownNouns["Pool"] = Noun::pool;
	knownNouns["POOL"] = Noun::pool;
	knownNouns["axe"] = Noun::axe;
	knownNouns["Axe"] = Noun::axe;
	knownNouns["AXE"] = Noun::axe;

	auto w = knownNouns.find(word);
	if (w == knownNouns.end())
		return Noun::invalid;

	return w->second;
}