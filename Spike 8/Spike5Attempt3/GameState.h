#ifndef GAMESTATE_H
#define GAMESTATE_H
class GameState
{
public: virtual ~GameState() {}
public: virtual void Update() {}
public: virtual void Draw () {}
};
#endif