#include <iostream>
#include <map>
#include <string>
#include <iterator>
#include <vector>
#include <sstream>
using namespace std;
void dostuff(string input)
{
	
	std::stringstream ss(input);
	std::istream_iterator<std::string> begin(ss);
	std::istream_iterator<std::string> end;
	std::vector<std::string> vstrings(begin, end);
	std::copy(vstrings.begin(), vstrings.end(), std::ostream_iterator<std::string>(std::cout, "\n"));
}

int main1()
{
	string input;

	cin >> input;
	dostuff(input);
	return 0;
}