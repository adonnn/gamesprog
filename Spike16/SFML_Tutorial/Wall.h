
#include <SFML\Graphics.hpp>
class Wall : public  virtual sf::Sprite {

public:
	sf::Sprite sprite;
	sf::Texture texture;
	void setWall(int x, int y, int walllength, int wallheight);


private:
	int walllength;
	int wallheight;
	int wallwidth;
};
