#include "Wall.h"
#include <SFML\Graphics.hpp>
#include <SFML\System.hpp>
using namespace std;

 void Wall::setWall(int x, int y, int walllength, int wallheight)
{
	
	if (!texture.loadFromFile("wall.png", sf::IntRect(0, 0, walllength, wallheight)))
	{
		//...
	}
	
	
	sprite.setTexture(texture);
	sprite.setPosition(sf::Vector2f(x, y));
	
}
