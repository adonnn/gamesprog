#include "SFML/Graphics.hpp"
#include <iostream>
#include <SFML\Wall.h>
#include <math.h>
#include <cmath>
#include <chrono>
#include <thread>
using namespace std;


bool Collision(sf::CircleShape* a, sf::CircleShape* b) {
	float a_square = pow(a->getPosition().x - b->getPosition().x, 2);
	float b_square = pow(a->getPosition().y - b->getPosition().y, 2);

	if (sqrt(a_square + b_square) >=
		a->getRadius() + b->getRadius()) {
		return false;
	}
	else {
		a->move(a->getPosition().x - b->getPosition().x, a->getPosition().y - b->getPosition().y);
		return true;
	}
}

int main()
{




	sf::CircleShape circle1;
	circle1.setRadius(40);
	circle1.setFillColor(sf::Color::Red);
	circle1.setPosition(400, 300);


	sf::CircleShape circle2;
	circle2.setRadius(40);
	circle2.setFillColor(sf::Color::Blue);
	circle2.setPosition(500, 300);





	sf::RenderWindow window(sf::VideoMode(1100, 700), "Sprite16!");

	sf::Texture texture;
	sf::Texture walltexture;
	if (!texture.loadFromFile("wall.png"))
	{
		//...
	}

	sf::Font font;
	if (!font.loadFromFile("arial.ttf")) {
		cout << "error" << endl;
	}

	if (!texture.loadFromFile("pizza.png"))
	{
		cout << "error" << endl;
	}

	sf::Sprite sprite;
	sf::Sprite leftwall;
	sf::Sprite bottomwall;
	sf::Sprite topwall;
	sf::Sprite rightwall;
	sf::Sprite sprite2;

	leftwall.setTexture(walltexture);
	leftwall.setTextureRect(sf::IntRect(0, 0, 10, 700));
	leftwall.setPosition(sf::Vector2f(0, 0));


	rightwall.setTexture(walltexture);
	rightwall.setTextureRect(sf::IntRect(0, 0, 10, 700));
	rightwall.setPosition(sf::Vector2f(1090, 0));

	topwall.setTexture(walltexture);
	topwall.setTextureRect(sf::IntRect(0, 0, 1100, 10));
	topwall.setPosition(sf::Vector2f(0, 0));

	bottomwall.setTexture(walltexture);
	bottomwall.setTextureRect(sf::IntRect(0, 0, 1100, 10));
	bottomwall.setPosition(sf::Vector2f(0, 690));


	sprite.setTexture(texture);
	sprite.setPosition(sf::Vector2f(200, 40));

	sprite2.setTexture(texture);
	sprite2.setPosition(sf::Vector2f(0, 400));


	int collisions;
	collisions = 2;

	while (window.isOpen())
	{
		sf::Event event;
		sf::Event CloseEvent;
		while (window.pollEvent(event))
		{

			switch (event.type)
			{
			case sf::Event::Closed:
				window.close();

				break;
			}
		}


		sf::Vector2f position = sprite2.getPosition();
		if (sprite2.getGlobalBounds().intersects(bottomwall.getGlobalBounds()))
		{
			collisions = 1;
		}

		if (sprite2.getGlobalBounds().intersects(topwall.getGlobalBounds()))
		{
			collisions = 3;
		}
		if (sprite2.getGlobalBounds().intersects(rightwall.getGlobalBounds()))
		{
			collisions = 4;
		}
		if (sprite2.getGlobalBounds().intersects(leftwall.getGlobalBounds()))
		{
			collisions = 5;
		}
		switch (collisions) {

		case 1:
			sprite2.move(sf::Vector2f(-0.5, -0.35));

			break;

		case 2:
			sprite2.move(sf::Vector2f(0.5, 0.7));
			break;


		case 3:
			sprite2.move(sf::Vector2f(0.5, 0.24));
			break;
		case 4:
			sprite2.move(sf::Vector2f(-0.3, 0.24));
			break;
		case 5:
			sprite2.move(sf::Vector2f(0.5, -0.35));
			break;
		}
		if (sprite.getGlobalBounds().intersects(sprite2.getGlobalBounds()))
		{


			sf::RenderWindow Collision1(sf::VideoMode(200, 200), "Sprite16!");

			sf::Text myText("Collision detected!", font, 25);

			while (Collision1.isOpen())
			{

				while (Collision1.pollEvent(CloseEvent))
				{

					if (CloseEvent.type == sf::Event::Closed)
					{
						Collision1.close();
						return 0;
					}

				}
				Collision1.clear();
				Collision1.draw(myText);
				Collision1.display();

			}
		}
		//else if (sprite.getGlobalBounds().intersects(window.getGlobalBounds()))
		//{

//		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) {
			circle1.move(-1, 0);
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) {
			circle1.move(1, 0);

		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) {
			circle1.move(0, -1);

		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) {
			circle1.move(0, 1);

		}

		if (Collision(&circle1, &circle2)) {

			std::chrono::milliseconds dura(100);
			window.clear();
			circle1.setFillColor(sf::Color::Yellow);
			window.draw(circle1);
			window.draw(circle2);
			window.draw(sprite);
			window.draw(sprite2);
			window.draw(leftwall);
			window.draw(bottomwall);
			window.draw(topwall);
			window.draw(rightwall);
			window.display();
			this_thread::sleep_for(dura);
		}
		else
		{

			circle1.setFillColor(sf::Color::Red);
		
		}

			window.clear();
			window.draw(circle1);
			window.draw(circle2);
			window.draw(sprite);
			window.draw(sprite2);
			window.draw(leftwall);
			window.draw(bottomwall);
			window.draw(topwall);
			window.draw(rightwall);
			window.display();
			
		}
	}
