#include <SFML\Graphics.hpp>
#include <iostream>
#include <SFML\Graphics.hpp>
#include <SFML\System.hpp>
#include <SFML\System.hpp>

using namespace std;

int main()
{

	sf::RenderWindow window(sf::VideoMode(500, 500), "SFML works!");
	sf::Texture texture;
	sf::Texture background;

		if (!texture.loadFromFile("pizza.png", sf::IntRect(0, 0, 300, 300)))
	{
		cout << "error" << endl;
	}

	if (!background.loadFromFile("background.png", sf::IntRect(0, 0, 500, 500)))
	{
		cout << "error" << endl;
	}


	sf::Image backgroundimage;
	sf::Sprite backgroundsprite;
	sf::Image image;
	sf::Sprite sprite1;
	sf::Sprite sprite2;
	sf::Sprite sprite3;



	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
		}
		//sf::Uint8* pixels = new sf::Uint8[200 * 200 * 4];
		//texture.update(pixels);
		backgroundsprite.setTexture(background);
		sprite1.setTexture(texture);
		sprite2.setTexture(texture);
		sprite3.setTexture(texture);
		sprite1.setTextureRect(sf::IntRect(0,0,100,300)); 
		sprite2.setTextureRect(sf::IntRect(100, 0, 100, 300));
		sprite3.setTextureRect(sf::IntRect(200, 0,100, 300));

		backgroundsprite.setPosition(sf::Vector2f(0, 0));
		sprite1.setPosition(sf::Vector2f(rand() % 400, rand() % 200));
		sprite2.setPosition(sf::Vector2f(rand() % 400, rand() % 200));
		sprite3.setPosition(sf::Vector2f(rand() % 400, rand() % 200));

		window.clear();
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Num0))
		{
			window.draw(backgroundsprite);
		}
		
		while (sf::Keyboard::isKeyPressed(sf::Keyboard::Num1))
		{
			
			window.draw(sprite1);
			window.display();
		}
		while (sf::Keyboard::isKeyPressed(sf::Keyboard::Num2))
		{
			window.draw(sprite2);
			window.display();
		}
		while (sf::Keyboard::isKeyPressed(sf::Keyboard::Num3))
		{
			window.draw(sprite3);
			window.display();
		}


		window.display();
	}

	return 0;
}