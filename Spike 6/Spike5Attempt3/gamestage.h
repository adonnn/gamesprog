#pragma once
#include "GameState.h"
#include "ItemClass.h"
#include "inventoryclass.h"

#include <iostream>
#include <string>
#include <list>
#include <typeinfo>
#include <vector>
class gamestage
{
public:

	gamestage(string area, string areanumber, string description);
	gamestage();
	string getareanum() { return areanumber; }
	string getarea() {return area; }
	string getdescription() { return description; }

	void setareanum(string input);
	void setarea(string input);
	void setdescription(string input);

	const string& getarea() const
	{
		return this->area;
	}
	const string& getnum() const
	{
		return this->areanumber;
	}
	const string& getdesc() const
	{
		return this->description;
	}


	~gamestage();

private:
	string area;
	string areanumber;
	string description;

};
