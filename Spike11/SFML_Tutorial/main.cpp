#include <SFML\Graphics.hpp>
int main()
{
	sf::RenderWindow window(sf::VideoMode(600, 800), "Spike11");

	while (window.isOpen())
	{
		sf::Event event;
		sf::Event CloseEvent;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed) 
			{

				sf::RenderWindow closewindow(sf::VideoMode(200, 200), "You've selected to close window");
				while(closewindow.isOpen())
				{
					while (closewindow.pollEvent(CloseEvent))
					{
						if (CloseEvent.type == sf::Event::Closed)
						{
							closewindow.close();
						}
					
					}
					
					window.clear();

					window.display();
				}
				window.close();
			}

		}

		window.clear();

		window.display();
	}

	return 0;
}