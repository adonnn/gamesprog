#pragma once
#include "gamestage.h"
#include <fstream>
#include <iostream>
#include <string>
#include "Enemies.h"
using namespace std;
class fileread
{
public:
	void previewread(gamestage &input, int increment);
	void filereads(gamestage &input, int increment);
	void readEnemies(Enemies &input, int increment);
	fileread();
	~fileread();
};

