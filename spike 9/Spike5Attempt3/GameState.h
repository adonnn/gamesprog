#ifndef GAMESTATE_H
#define GAMESTATE_H
#include "GameState.h"
class GameState
{
public: virtual ~GameState() {}
public: virtual void Update() {}
public: virtual void Draw () {}
};
#endif