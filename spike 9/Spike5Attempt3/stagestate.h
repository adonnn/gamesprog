#pragma once
#include "gamestage.h"
#include <iostream>
#include <string>
#include <list>
#include <typeinfo>
#include <vector>
#include <map>
#include "verb.h"
#include "inputClass.h"
#include "Noun.h"
using namespace std;
class Entity;
class stagestate
{
public:
	string input = "0";
	vector <gamestage> stage;

	int Draw();
	void printstage(gamestage &statesofgame);
	void addStage(gamestage &thestage);
	void previewstage(gamestage &statesofgame, gamestage &statesofgame2);
	stagestate();
	~stagestate();

};