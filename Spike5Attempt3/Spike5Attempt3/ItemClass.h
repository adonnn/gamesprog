#ifndef ITEMCLASS_H
#define ITEMCLASS_H
#include <iostream>
#include <string>
using namespace std;
class ItemClass {
public:

	string getItemnum() { return ItemNum; }
	string getItemName() { return ItemName; }
	string getItemStrength() { return ItemStrength; }
	ItemClass(string, string, string);
	ItemClass();
	~ItemClass();
	void toString();

	void setName(string name) { ItemName = name; }
	void setNum(string num) { ItemNum = num; }
	void setStrength(string strength) { ItemStrength = strength; }
	

	const string getName() const
	{
		return ItemName;
	}
	 const string& getNum() const
	 {
		 return this->ItemNum;
	 }
	 const string& getStrength() const
	 {
		 return this->ItemStrength;
	 }	
	 
	private:
	
		string ItemNum;
		string ItemName;
		string ItemStrength;
};
	
#endif