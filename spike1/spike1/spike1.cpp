// spike1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"


#include <iostream>
#include <string>
using std::cout;
using std::endl;
using std::cin;
using std::string;
using namespace std;
int SX = 2;
int SY = 8;
int changelocation(std::string input)
{
	int movement = 0;

	if (input == "N" || input == "n")
	{
		movement = 1;
	}
	if (input == "E" || input == "e")
	{
		movement = 2;
	}
	if (input == "S" || input == "s")
	{
		movement = 3;
	}
	if (input == "W" || input == "w")
	{
		movement = 4;
	}
	if (input == "Start" || input == "start")
	{
		movement = 5;
	}
	return movement;


}

void displaymap(std::string b[8][8], int input)
{
	std::string P = "P";
	int c;
	int d;
	d = 0;
	c = 0;

	if (input == 1)
	{
		--SY;
	}

	if (input == 2)
	{
		++SX;
	}
	if (input == 3)
	{
		++SY;
	}

	if (input == 4)
	{
		--SX;
	}
	if (input == 5)
	{
		SX = 2;
		SY = 7;
	}

	if (b[SY][SX] == "#")
	{
		cout << "Your road is blocked! Choose another path!" << endl;
		return;
	}
	if (b[SY][SX] == "D")
	{
		cout << "You Died! Please type 'Start' To try Again or Q to quit" << endl;
		SY = 8;
		SX = 2;
		b[SY][SX] == "P";
		return;
	}
	if (SY >= 0 && SY <= 8)
	{
		if (SX >= 0 && SY <= 8)
		{
			b[SY][SX] = "P";
		}

	}


	for (c = 0; c < 8; c++)
	{
		for (d = 0; d < 8; d++)
		{
			std::cout << b[c][d];
		}
		if (d == 8) { d = 0; cout << "\n"; };

	}
	b[SY][SX] = "+";

	return;
}

int main()
{

	int direction;
	std::string playerinput;
	std::string Q = "Q";
	std::string a[8][8] = {
		{ "#","#","#","#","#","#","#","#" } ,
		{ "#","G"," ","D","#","D"," ","#" } ,
		{ "#"," "," "," ","#"," "," ","#" } ,
		{ "#","#","#"," ","#"," ","D","#" } ,
		{ "#"," "," "," ","#"," "," ","#" } ,
		{ "#"," ","#","#","#","#"," ","#" } ,
		{ "#"," "," "," "," "," "," ","#" } ,
		{ "#","#","S","#","#","#","#","#" }
	};

	cout << "Welcome to GridWorld: Quantised Excitement. Fate is waiting for You!\n""Valid commands: N, S, E and W for direction. Q to quit the game. Type 'Start' to start the game!\n" << endl;
	while (playerinput != Q)
	{
		if (SX == 1 && SY == 1)
		{
			cout << "Win!" << endl;
		
			return 0;
		}
		std::cin >> playerinput;
		cout << playerinput << endl;
		direction = changelocation(playerinput);
		cout << "\n" << endl;
		cout << SX << endl;
		cout << SY << endl;
		displaymap(a, direction);
	}



	return 0;
}




