#include "stagestate.h"
#include "gamestage.h"
#include <fstream>
#include <string>
#include <iostream>
#include "fileread.h"
#include "Enemies.h"
#include "MenueGameState.h"
using namespace std;

stagestate::stagestate()
{

}


int stagestate::Draw()
{

	stagestate statemanager;
	vector <gamestage> statesofgame;

	fileread file;

	//gamestage house("House", "0", "Scary house");
	//file.filereads(house);



	gamestage* house1;
	gamestage house;
	gamestage* pool1;
	gamestage pool;
	gamestage* backhouse1;
	gamestage backhouse;
	gamestage* insidehouse1;
	gamestage insidehouse;
	int increment = 0;

	house1 = &house;
	pool1 = &pool;
	backhouse1 = &backhouse;
	insidehouse1 = &insidehouse;

	std::map<std::string, verb > knownVerbs;
	knownVerbs["take"] = verb::take;
	knownVerbs["go"] = verb::go;

	std::map<std::string, Noun > knownNouns;
	knownNouns["House"] = Noun::house;
	knownNouns["house"] = Noun::house;
	knownNouns["backyard"] = Noun::backhouse;
	knownNouns["backhouse"] = Noun::backhouse;
	knownNouns["backdoor"] = Noun::backhouse;
	knownNouns["pool"] = Noun::pool;
	knownNouns["Pool"] = Noun::pool;


	Enemies frog;
	Enemies* frog1;
	int counter = 1;
	frog1 = &frog;
	file.readEnemies(*frog1, counter);

	verb Verb;
	Noun noun;
	Noun noun2;


	string* word;
	string inputword;
	word = &inputword;


	inputClass inputclassinput;

	
	

	
	inventoryclass player;

	ItemClass axe("1", "Axe", "40");
	ItemClass note("2", "Note,", "0");


	file.filereads(*house1, increment);
	file.~fileread();
	increment++;
	file.filereads(*pool1,increment);
	increment++;
	file.~fileread();
	file.filereads(*backhouse1,increment);
	increment++;
	file.~fileread();
	file.filereads(*insidehouse1, increment);
	increment++;
	file.~fileread();

	

	
	statesofgame.push_back(*house1);
	statesofgame.push_back(*pool1);
	statesofgame.push_back(*backhouse1);
	statesofgame.push_back(*insidehouse1);
	int itemcheckaxe = 0;
//	statemanager.printstage(statesofgame[0]);
	//statemanager.printstage(statesofgame[1]);
	//statemanager.printstage(statesofgame[2]);
	cout << "----------------" << endl;

	//gamestage pool("Pool", "1", "Pool filled with blood");
	//gamestage backofhouse("Back of house", "2", "Back of house has a door and a axe");
	
	//statesofgame.push_back(pool);
	//statesofgame.push_back(backofhouse);
	
	
	cout << " Welcome to the game! starting location is house, goodluck!" << endl;
	statemanager.printstage(statesofgame[0]);
	statemanager.previewstage(statesofgame[1], statesofgame[2]);
	cin >> inputword;
	Verb = inputclassinput.parseVerb(*word);
	cin >> inputword;
	noun = inputclassinput.parseNoun(*word);


	int a=0;
	int b=1;
	int c=2;
	while (inputword != "Quit") 
	{

		if (Verb == verb::go && noun == Noun::house)
		{
			statemanager.printstage(statesofgame[0]);
		
			statemanager.previewstage(statesofgame[1], statesofgame[2]);
		
			cin >> inputword;
			Verb = inputclassinput.parseVerb(*word);
			cin >> inputword;
			noun = inputclassinput.parseNoun(*word);
			cin >> inputword;
			noun2 = inputclassinput.parseNoun(*word);

			if (Verb == verb::use && noun == Noun::axe && noun2 == Noun::door)
			{
				if (itemcheckaxe == 1) {
					cout << "YOU BROKE THE DOOR DOWN!" << endl;
					statemanager.printstage(statesofgame[3]);

					statemanager.previewstage(statesofgame[0], statesofgame[2]);
					a = 3;
					b = 0; c = 2;

					cin >> inputword;
					Verb = inputclassinput.parseVerb(*word);
					cin >> inputword;
					noun = inputclassinput.parseNoun(*word);

					if (Verb == verb::take && noun == Noun::note)
					{
						cout << "You picked up the note!" << endl;
						player.addItem(note);
						player.printInventory();
						cin >> inputword;
						Verb = inputclassinput.parseVerb(*word);
						cin >> inputword;
						noun = inputclassinput.parseNoun(*word);
					}

				}
				

			}
			if ((Verb == verb::use) && (itemcheckaxe==0))
			{
				cout << "you dont have anything to hit the door with!" << endl;
				statemanager.printstage(statesofgame[0]);
				a = 0;
				statemanager.previewstage(statesofgame[1], statesofgame[2]);
				b = 1;
				c = 2;
				cin >> inputword;
				Verb = inputclassinput.parseVerb(*word);
				cin >> inputword;
				noun = inputclassinput.parseNoun(*word);
			}

		}

		else if ((Verb == verb::go && noun == Noun::pool)|| (Verb == verb::stay && noun == Noun::pool))
		{
			statemanager.printstage(statesofgame[1]);
			statemanager.previewstage(statesofgame[0], statesofgame[2]);
			a = 1;
			b = 0; c = 2;

			if (frog.Alive)
			{
				cout << "A Wild Frog is front of you!" << endl;
				cout << "Its Health is " << frog.health << "!" << endl;

			
			}


			cin >> inputword;
			Verb = inputclassinput.parseVerb(*word);
			cin >> inputword;
			noun = inputclassinput.parseNoun(*word);
			cin >> inputword;
			noun2 = inputclassinput.parseNoun(*word);

			cout << "---------------------------------" << endl;

			if (Verb == verb::punch && noun == Noun::frog)
			{
				if (frog.health > 0)
				{

					cout << "you punch the frog! \n" << endl;
					cout << "the frog counter attacks with a bite!\n " << endl;
					frog.health -= 10;
					player.health -= frog.damageDelt1;
					cout << "frogs health is: " << frog.health << endl;
					
					cout << "your health is: " << player.health << endl;

				}
				if (frog.health < 0)
				{

					frog.Alive = false;
					cout << " you clobber the frog in the face and kill it!\n" << endl;
					cout << "you've manage to slay the only enemy in the game!" << endl;

				}
			}


			if (Verb == verb::use && noun == Noun::axe && noun2 == Noun::frog)
			{
				if (itemcheckaxe == 1)
				{
					if (frog.health > 0)
					{

						cout << "you swing your axe and hit the frog! \n" << endl;
						cout << "the frog counter attacks with a bite!\n " << endl;
						frog.health -= 40;
						player.health -= frog.damageDelt1;
						cout << "frogs health is: " << frog.health << endl;

						cout << "your health is: " << player.health << endl;

					}
					if (frog.health < 0)
					{
					
					frog.Alive = false;
					cout << " You swing at the frog with your axe and hit a vital spot.\n" << endl;
					cout << "you've manage to slay the only enemy in the game!" << endl;

					}
				}
				else {
					cout << "You dont have any weapons to use!" << endl;
					cout << " in your blunder the frog bites hard!" << endl;

					player.health -= frog.damageDelt1;
					cout << "player health: " << player.health << endl;
				}
				cout << "----------------------------------------" << endl;

				if (player.health <= 0) {
					cout << " You've Died!!!" << endl;

					MenueGameState menu;
					menu.Draw();
					return 0;
				}
				if (Verb == verb::invalid || noun == Noun::invalid || noun2 == Noun::invalid) {
					cout << "PLEASE INPUT A VALID INPUT\n" << endl;
					statemanager.printstage(statesofgame[1]);
					statemanager.previewstage(statesofgame[0], statesofgame[2]);
					cout << "where would you like to go?" << endl;
					cin >> inputword;
					Verb = inputclassinput.parseVerb(*word);
					cin >> inputword;
					noun = inputclassinput.parseNoun(*word);
					//cin >> inputword;
					//noun2 = inputclassinput.parseNoun(*word);
				}

			
			}
		}

		else if (Verb == verb::go && noun == Noun::backhouse)
		{

			statemanager.printstage(statesofgame[2]);
			statemanager.previewstage(statesofgame[1], statesofgame[0]);

			a = 2;
			b = 1; c = 0;
			cin >> inputword;
			Verb = inputclassinput.parseVerb(*word);
			cin >> inputword;
			noun = inputclassinput.parseNoun(*word);

			if (Verb == verb::take && noun == Noun::axe)
			{
				cout << "You picked up the Axe!" << endl;
				player.addItem(axe);
				player.printInventory();
				itemcheckaxe = 1;
				cin >> inputword;
				Verb = inputclassinput.parseVerb(*word);
				cin >> inputword;
				noun = inputclassinput.parseNoun(*word);
			}

	

		}

		else 
		{

		//	cout << "PLEASE INPUT A VALID SELECTION\n" << endl;

			statemanager.printstage(statesofgame[a]);
			statemanager.previewstage(statesofgame[b], statesofgame[c]);
			cin >> inputword;
			Verb = inputclassinput.parseVerb(*word);
			cin >> inputword;
			noun = inputclassinput.parseNoun(*word);

	
	
		}
	}

	MenueGameState menu;
	menu.Draw();
	return 0;
	
}




void stagestate::addStage(gamestage &thestage)
{
	stage.push_back(thestage);
}

void stagestate::printstage(gamestage &statesofgame)
{	
	cout << "current Number: " << statesofgame.getnum() << "\n" << endl;
	cout << "current area: " << statesofgame.getarea() << "\n" <<endl;
	cout << "Area Description: " << statesofgame.getdesc() << "\n" << endl;
	
	cout <<   endl;
}
void stagestate::previewstage(gamestage &statesofgame,gamestage &statesofgame2)
{
	cout << "Area to right Number: " << statesofgame.getnum() << "Area: " << statesofgame.getarea() << endl;
	cout << "Area to left Number: " << statesofgame2.getnum() << "Area: " << statesofgame2.getarea() << endl;
	cout << "please type 'go' and then your desired location" << endl;
	cout << "--------------------------------------------------------" << endl;
}
	

stagestate::~stagestate()
{
}
