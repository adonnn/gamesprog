#pragma once
#include "Entity.h"
#include <iostream>
#include <string>
class Enemies : public Entity
{
public:
	const string& getname() const
	{
		return this->name;
	}
	const int& gethealth() const
	{
		return this->health;
	}

	void setName(string input);
	void sethealth(string input);
	Enemies();
	~Enemies();
	bool Alive = true;
	int damageDelt1 = 30;
	int health;

private: 
	
	string name;
};

